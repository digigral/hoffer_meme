
$(document).ready(function () {
	grecaptcha.ready(function() {
		grecaptcha.execute('6LejHT0aAAAAAMuNWT27U6MTOq7VcbOUOif0GDpf', {action: 'validate_captcha'}).then(function(token) {
			document.getElementById('g-recaptcha-response').value = token;			

		});
	});
	$('input[type=file]').change(function(event) {
		const fileName = event.target.files[0].name;

		$('#uploadImgButton').addClass('hidden');
		$('#loader-wrapper').addClass('active');

		setTimeout(function() {
			$('#loader-wrapper').fadeOut();

			if(fileName) {
				$('#filename').html(fileName);
			} else {
				$('#filename').html('');	
			}
		}, 5000);
	});

	$("#memesUpload").on('submit', (function (e) {
		const submitButton = this;
		e.preventDefault();
		$.ajax({
			url: "ajaxupload.php",
			type: "POST",
			data: new FormData(submitButton),
			contentType: false,
			cache: false,
			processData: false,
			beforeSend: function () {
				$("#err").fadeOut();
			},
			success: function (data) {
				console.log(data);
				if (data == 'tooBig') {
					// Too big format
					$("#err").html("Datoteka je prevelika!").fadeIn();
					$('#uploadImgButton').fadeIn();
				} else if(data == 'invalid') {
					// Invalid file format.
					$("#err").html("Neveljavna oblika datoteke!").fadeIn();
					$('#uploadImgButton').fadeIn();
				}
				else {
					console.log('test');
					$("#memesUpload").hide();
					$("#err").hide();
					$(".tankyou-page").show();
				}
			},
			error: function (e) {
				$("#err").html(e).fadeIn();
			}
		});
	}));
});

function invalidMsg(textbox) {
	if (textbox.value == '') 
		textbox.setCustomValidity('To polje je obvezno.');

	else if(textbox.validity.typeMismatch)
		textbox.setCustomValidity('To polje je obvezno.');
	else 
		textbox.setCustomValidity('');
	return true;
}	